package sea;

import java.awt.image.BufferedImage;


///Класс, описывающий игровой объект
public class SeaObject {
    private int posX;
    private int posY;
    private BufferedImage image;

    public SeaObject(BufferedImage image, int posX, int posY) {
        this.image = image;
        this.posX = posX;
        this.posY = posY;
    }

    ///Метод получения картинки
    public BufferedImage getImage() {
        return this.image;
    }

    ///Позиция по х
    public int getPosX() {
        return this.posX;
    }

    ///Позиция по у
    public int getPosY() {
        return this.posY;
    }

    ///Смещение объекта
    public void move(int deltaX, int deltaY) {
        this.posX += deltaX;
        this.posY += deltaY;
    }
}
          
