package tests;

import org.junit.Test;
import sea.SeaObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class SeaTest {
    @Test
    public void testMove() throws IOException {
        System.out.println(new File(".").getAbsolutePath());
        File f = new File("image/pirategirl.png");
        BufferedImage playerImage = ImageIO.read(f); 
        Point position = new Point(0, 0);
        SeaObject obj = new SeaObject(playerImage, position.x, position.y);
        assertEquals(position.x, obj.getPosX());
        assertEquals(position.y, obj.getPosY());
    }
}
